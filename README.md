# dotfiles

## Setup
```sh
rm -rf $HOME/.dotfiles.git
repo=https://gitlab.com/zoltan-cs/dotfiles.git
git clone --bare $repo $HOME/.dotfiles.git
dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"
$dotfiles config --local status.showUntrackedFiles no
$dotfiles checkout
# $dotfiles checkout -f
```

## Usage
- `alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"` use it instead of the git command (like `dotfiles add filename, dotfiles commit -m "msg", dotfiles push`)
- `dotfiles checkout` restore working tree files (check local changes)
- `dotfiles checkout -f` force restore working tree files (throw away local changes)

## Info
- [Arch wiki](https://wiki.archlinux.org/index.php/Dotfiles)
