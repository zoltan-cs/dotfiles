# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Prompt
esc=$(printf '\e[')
bold="${esc}1m"
reset="${esc}0m"
# brgybmcw: fore 30m-37m, back 40m-47m, hi-fore 90m-97m,, hi-back 100m-107m
green="${esc}32m"

PS1="\[$green\]\[$bold\]\u@\h:\w\n\[$reset\]\\$ "

# Basic vars
HISTCONTROL=ignoreboth # no duplicate lines or lines starting with space
HISTSIZE=1000 # for setting history length
HISTFILESIZE=2000 # for setting history length
shopt -s checkwinsize # update the values of LINES and COLUMNS.
shopt -s histappend # append to the history file, don't overwrite it
set -o vi
export TERM=st-256color # for tmux colors

# Path
#for dir in $HOME/bin $HOME/bin/*/; do
for dir in $HOME/bin; do
  [ -d $dir ] && PATH=${PATH}:$dir
done

# Aliases
alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -A'
alias grep='grep --color=auto'
alias gacp='git add . && git commit -m "." && git push'
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"
alias dotfilescp='dotfiles commit -m "." && dotfiles push'

# Bash-completion
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Functions
ranger() {
	[ -z "$RANGER_LEVEL" ] && /usr/bin/ranger "$@" || exit
}

