#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0) number
number: 1=10%, ... , 9=90%, 0=100%
Run as root

EOF

exit 1
}

[ -e /sys/class/backlight/*/brightness ] || \
  { echo "There is no /sys/class/backlight/*/brightness to set"; exit 1; }

[ $(id -u) -eq 0 ] && [ $# -eq 1 ] || usage
case $1 in
  [0-9]) pctnew=$1;;
  *) usage;;
esac

max=$(cat /sys/class/backlight/*/max_brightness)
pct10=$(expr $max / 10)
current=$(ls /sys/class/backlight/*/brightness)

if [ $1 -eq 0 ]; then
  echo $max > $current
else
echo $pctnew
echo $pct10
  echo $(expr $pctnew \* $pct10) > $current
fi

