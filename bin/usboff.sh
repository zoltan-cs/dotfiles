#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0) /dev/sdX
Run as root

EOF

lsblk | grep -v loop
exit 1
}

[ $(id -u) -eq 0 ] && [ $# -eq 1 ] || usage
case $1 in
  /dev/sd[b-z]) [ -b $1 ] || usage;;
  *) usage;;
esac

umount $1*
udisksctl power-off -b $1

