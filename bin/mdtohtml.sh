#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0) filename.md

EOF

exit 1
}

[ $# -eq 1 ] && [ -f "$1" ] || usage

printf "%s\n" \
  '<head>' \
  '<link rel="stylesheet" type="text/css" href="mydoc.css">' \
  '</head>' \
  '' > "$1.html"

markdown "$1" >> "$1.html"
