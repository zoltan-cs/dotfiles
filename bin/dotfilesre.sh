#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0)
Do not as root

EOF

exit 1
}

[ $(id -u) -ne 0 ] || usage

rm -rf $HOME/.dotfiles.git
repo=https://gitlab.com/zoltan-cs/dotfiles.git
git clone --bare $repo $HOME/.dotfiles.git
dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"
$dotfiles config --local status.showUntrackedFiles no
$dotfiles checkout -f
