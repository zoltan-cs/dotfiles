#!/bin/sh

xmenu <<EOF | sh &
0%	pactl set-sink-volume @DEFAULT_SINK@ 0%
10%	pactl set-sink-volume @DEFAULT_SINK@ 10%
20%	pactl set-sink-volume @DEFAULT_SINK@ 20%
30%	pactl set-sink-volume @DEFAULT_SINK@ 30%
40%	pactl set-sink-volume @DEFAULT_SINK@ 40%
50%	pactl set-sink-volume @DEFAULT_SINK@ 50%
60%	pactl set-sink-volume @DEFAULT_SINK@ 60%
70%	pactl set-sink-volume @DEFAULT_SINK@ 70%
80%	pactl set-sink-volume @DEFAULT_SINK@ 80%
90%	pactl set-sink-volume @DEFAULT_SINK@ 90%
100%	pactl set-sink-volume @DEFAULT_SINK@ 100%
Pavucontrol	pavucontrol
EOF
