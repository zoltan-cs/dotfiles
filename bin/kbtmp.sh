#!/bin/sh

# empty -option param - to clear the options
# setxkbmap -query - to list settings 
# /usr/share/X11/xkb/rules/base.lst
setxkbmap -layout "us,hu" -option
setxkbmap -option "compose:ralt,grp:shifts_toggle"
