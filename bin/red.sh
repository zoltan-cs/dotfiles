#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0) option
options:
n       - night, 1500
d       - daylight, 6500
4digits - exact value

EOF

exit 1
}

[ $# -eq 1 ] || usage
case $1 in
  "n") lvl=1500;;
  "d") lvl=6500;;
  [1-9][0-9][0-9][0-9]) lvl=$1;;
  *) usage
esac

redshift -P -O $lvl

