#!/bin/sh

localectl set-keymap --no-convert "us"
localectl set-x11-keymap --no-convert "us,hu" "pc105" "" "compose:ralt,grp:shifts_toggle"
