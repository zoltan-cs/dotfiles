#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0)
Run as root

EOF

exit 1
}

[ $(id -u) -eq 0 ] || usage

pacman -Syu
pacman -S qemu virt-manager dmidecode dnsmasq iptables-nft edk2-ovmf 

systemctl enable libvirtd
systemctl start libvirtd

usermod -G libvirt -a $(logname)

cat << EOF

On Win guest: spice-space.org, spice-guest-tools

Need to reboot

EOF
