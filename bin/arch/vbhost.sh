#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0)
Run as root

EOF

exit 1
}

[ $(id -u) -eq 0 ] || usage

pacman -Syu
pacman -S virtualbox-host-modules-arch virtualbox
usermod -aG vboxusers,disk $(logname)

echo "Need to reboot"
