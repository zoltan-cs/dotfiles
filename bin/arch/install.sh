#!/bin/bash

### Variables
keymap="us"
mirror=$(cat << 'EOF'
Server = https://ftp.ek-cer.hu/pub/mirrors/ftp.archlinux.org/$repo/os/$arch
Server = https://quantum-mirror.hu/mirrors/pub/archlinux/$repo/os/$arch
EOF
)
l_time="Europe/Budapest"
l_gen="en_US.UTF-8 UTF-8"
l_conf="LANG=en_US.UTF-8" 
swap="2048"


### Functions
usage() {
cat << EOF

Usage: $(basename $0) device hostname username

- device is like /dev/sda, /dev/vda, ...
- only UEFI
- internet connection is required
- run like: bash $(basename $0)

EOF

lsblk
exit 1
}

clean_up() {
  ret=$?; set +e; trap '' 0
  stty echo
  umount -f /mnt 2>/dev/null
  umount -f /mnt/boot 2>/dev/null && rmdir /mnt/boot
  exit $ret
}

pre_chroot() {
  # Set the keyboard layout
  loadkeys $keymap

  # Verify the boot mode, internet connection
  [ -d /sys/firmware/efi/efivars ] || usage
  curl -Is google.com >/dev/null || usage

  # Update the system clock
  timedatectl set-ntp true

  # Ask password
  stty -echo
  printf "\nPassword for root, $un: "; read pw
  stty echo
  printf "\n\n"

  # Install keyring
  pacman -S archlinux-keyring

  # Partition the disk
  lsblk $dev

  printf "\nAll data will be lost on $dev. Ok? [y/N]: "; read yn
  [ "$yn" = "y" ] || [ "$yn" = "Y" ] || exit 1

  sgdisk -Z $dev; sleep 2
  { printf "%s\n" "g" "n" "" "" "+1G" "t" "1" "w"; sleep 1; } | fdisk $dev
  { printf "%s\n" "n" "" "" "" "w"; sleep 1; } | fdisk $dev

  # Format the partitions 
  mkfs.fat -F 32 ${dev}1
  mkfs.ext4 -F ${dev}2

  # Mount the file systems
  mount ${dev}2 /mnt
  mkdir /mnt/boot
  mount ${dev}1 /mnt/boot

  # Select the mirrors
  echo "$mirror" > /etc/pacman.d/mirrorlist

  # Install essential packages
  pacstrap /mnt base linux base-devel systemd-sysvcompat

  # Fstab
  genfstab -U /mnt > /mnt/etc/fstab
}

chroot_basic() {
  # Time zone 
  ln -sf /usr/share/zoneinfo/$l_time /etc/localtime
  hwclock --systohc

  # Localization
  sed -i '/^#'"$l_gen"'/s/^#//' /etc/locale.gen
  locale-gen
  echo $l_conf > /etc/locale.conf
  echo KEYMAP=$keymap > /etc/vconsole.conf

  # Network Configuration
  echo $hn > /etc/hostname
  echo "127.0.0.1 localhost" > /etc/hosts
  echo "::1       localhost" >> /etc/hosts
  echo "127.0.1.1 $hn.localdomain $hn" >> /etc/hosts

  # Boot loader
  pacman --noconfirm --needed -S grub efibootmgr os-prober
  grub-install --target=x86_64-efi --efi-directory=/boot
  grub-mkconfig -o /boot/grub/grub.cfg

  # Swap file
  dd if=/dev/zero of=/swapfile bs=1M count=$swap status=progress
  chmod 600 /swapfile
  mkswap /swapfile
  swapon /swapfile
  echo "/swapfile none swap defaults 0 0" >> /etc/fstab

  # User
  pacman --noconfirm --needed -S sudo
  sed -i '/^#.%sudo/s/^#.//' /etc/sudoers
  groupadd sudo
  useradd -m -G sudo -s /bin/bash $un

  # SSD trim
  [ "" = "$(lsblk -d -o rota | grep 0)" ] || systemctl enable fstrim.timer
}

dotfiles() {
  dots="https://gitlab.com/zoltan-cs/dotfiles.git"
  /usr/bin/git clone --bare $dots ~/.dotfiles.git
  dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"
  $dotfiles config --local status.showUntrackedFiles no
  $dotfiles checkout -f
}

chroot_custom() {
  # Packages
  basic="xorg-server xorg-xinit xorg-xsetroot xorg-xprop polkit lxsession \
    arandr xdg-user-dirs git vim rxvt-unicode screen tmux"
  vendor="intel-ucode"
  net="networkmanager firefox deluge-gtk curl wget"
  media="pulseaudio pavucontrol mpv yt-dlp imagemagick feh"
  customize="ttf-liberation hicolor-icon-theme gnome-themes-extra"
  misc="man-db ranger pcmanfm gvfs rsync dosfstools ntfs-3g unzip unrar dmenu \
    slock mupdf acpi udisks2 discount redshift"

  pacman -S --noconfirm $basic

  msg="\n"
  for pkg in $vendor $net $media $customize $misc; do
    pacman -S --noconfirm $pkg || msg="$msg Error: $pkg\n"
  done

  su $un -c dotfiles

  # Suckless dwm
  cd /home/$un
  git clone https://git.suckless.org/dwm
  cd dwm
  cp /home/$un/.config/dwm/config.h ./
  make clean install

  sleep 1

  # Suckless st
  cd /home/$un
  git clone https://git.suckless.org/st
  cd st
  cp /home/$un/.config/st/config.h ./
  make clean install

  # Xmenu
  cd /home/$un
  git clone https://github.com/phillbush/xmenu
  cd xmenu
  make clean install

  systemctl enable NetworkManager

  printf "$msg\nLIVE LONG AND PROSPER\n\n"
}


### Main
trap 'clean_up' 0 1 2 9 15
set -e

[ $# -eq 3 ] || usage
[ -b $1 ] || usage
dev=$1; hn=$2; un=$3

pre_chroot

export hn un keymap dev mirror l_time l_gen l_conf swap

export -f chroot_basic 
arch-chroot /mnt bash -c chroot_basic

arch-chroot /mnt bash << EOF
echo "root:$pw" | chpasswd
echo "$un:$pw" | chpasswd
EOF

export -f dotfiles chroot_custom
arch-chroot /mnt bash -c chroot_custom

