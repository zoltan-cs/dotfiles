#!/bin/sh
bsn_scr=$(basename $0)
dir_scr=$(cd $(dirname $0); pwd -P)
mnt_fat="/mnt/usb_mnt_fat"

usage() {
cat <<- EOF
Usage: $bsn_scr /dev/sdX N
/dev/sdXN has to be removable, fat32 on mbr table. Run as root.

EOF

exit 1
}

### checks
[ $(id -u) -eq 0 ] && [ $# -eq 2 ] && [ -b $1 ] && [ -b $1$2 ] && \
  [ "$(lsblk $1 -o rm | grep '1')" ] && \
  [ "$(lsblk $1$2 -o fsver | grep 'FAT32')" ] && \
  [ "$(lsblk $1 -o pttype | grep 'dos')" ] || usage
[ ! "$(mount | grep '/mnt ')" ] || { echo "$bsn_scr: mnt is mounted"; exit 1; }

lsblk $1; printf "Using: $1$2. Continue? [y/N]: "; read yn
[ "$yn" = "y" ] || [ "$yn" = "Y" ] || exit 1

### packages, mount, del old files
pacman --needed -S grub efibootmgr
#apt-get install grub-pc-bin grub-efi-amd64-bin
umount $1$2
mkdir -p $mnt_fat
mount -v $1$2 $mnt_fat
rm -rf $mnt_fat/boot $mnt_fat/EFI 2 > /dev/null

### install grub, cfg
grub-install --target=i386-pc --boot-directory=$mnt_fat/boot --recheck $1
grub-install --target=x86_64-efi --boot-directory=$mnt_fat/boot --recheck \
  --efi-directory=$mnt_fat --removable
cp -rf $dir_scr/grub.cfg $dir_scr/grub.d $mnt_fat/boot/grub/

### cleanup
umount -f $mnt_fat 2> /dev/null
rm -rf $mnt_fat 2> /dev/null

