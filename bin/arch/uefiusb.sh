#!/bin/sh

usage() {
cat << EOF
Usage: $(basename $0) /dev/sdXX archlinux-YYYY.MM.DD-x86_64.iso
Run as root

EOF

lsblk | grep -v loop
exit 1
}

[ $(id -u) -eq 0 ] && [ $# -eq 2 ] && [ -f "$2" ] || usage
case $1 in
  /dev/sd[b-z][1-9]) [ -b $1 ] || usage;;
  *) usage;;
esac

mkdir -p /mnt/iso /mnt/usb
mount -o loop "$2" /mnt/iso
mount $1 /mnt/usb

cp -a /mnt/iso/* /mnt/usb
sync

umount /mnt/iso /mnt/usb
rm -rf /mnt/iso /mnt/usb

y=$(echo "$2" | cut -d"-" -f2 | cut -d"." -f1)
m=$(echo "$2" | cut -d"-" -f2 | cut -d"." -f2)
/sbin/fatlabel $1 "ARCH_$y$m"
