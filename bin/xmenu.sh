#!/bin/sh

xmenu <<EOF | sh &
Firefox	firefox
Chromium	chromium
Pcmanfm	pcmanfm
Terminal (st)	st
Terminal (urxvt)	urxvt
Virt-manager	virt-manager
Libreoffice	libreoffice
Vscode	code
Deluge	deluge

Shutdown	poweroff
Reboot	reboot
Quit dwm	pkill dwm
EOF

