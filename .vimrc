""" Basic
set nocompatible    " Use Vim settings, rather than Vi
set noswapfile 
set nobackup
set noundofile
set viminfo=        " No .viminfo files
set number
set relativenumber
set visualbell      " Use visual bell instead of beeping
set t_vb=           " No visual bell (blink)
set showcmd         " Display incomplete commands
set nowrap          " Long lines in one line
set mouse=a         " Enable mouse
syntax on           " Syntax colors
filetype on         " Filetype

""" White space
set ts=4 sw=4 et    " :help tabstop
set autoindent      " Auto indent new lines
set listchars=eol:$,tab:>-,trail:~

""" Search
set incsearch       " Do incremental searching (match while you type)
set nohlsearch      " No highligh search

""" Colorcolumn
set colorcolumn=80
hi ColorColumn ctermbg=none ctermfg=1

""" Find files
set path+=**        " Search down into subfolders
set wildmenu        " Display all matching files when we tab complete

""" Autocmd
autocmd BufNewFile,BufRead *{.sh,.cfg,.txt,rc,.md} set ts=2 sw=2

""" Statusline
hi StatusLine ctermbg=0 ctermfg=8
set laststatus=2
set statusline+=[%n]\ %{&ff}          " Buffer, file format (end of line)
set statusline+=\ %{&fenc?&fenc:&enc} " Encoding
set statusline+=\ %{&bomb?'+':'-'}    " BOM
set statusline+=\ %y\ %F%m%r          " Type, file path, modified, readonly
set statusline+=%=%l-%c\ %L\          " (%= the right align) Line, col

""" Keybind
nnoremap <Space>b :buffers<CR>:buffer<Space>
nnoremap <Space>j :
nnoremap <Space>f /
nnoremap <Space>tl :set list!<CR>


""" Etc
" :retab - Change all the existing tabs to match current settings
" :find filename - Search in subfolders (tab, jokers)
" alt + l is similar to esc in terminal
" to see \r - vim -b, or :e ++ff=unix
" change fileformat - :set ff:{unix,dos,mac}
